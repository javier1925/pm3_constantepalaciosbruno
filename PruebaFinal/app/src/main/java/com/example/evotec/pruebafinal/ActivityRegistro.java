package com.example.evotec.pruebafinal;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.evotec.pruebafinal.utilidades.utilidades;

public class ActivityRegistro extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        edtID = (EditText)findViewById(R.id.edtID);
        edtNombre = (EditText)findViewById(R.id.edtNombre);
        edtMateria = (EditText)findViewById(R.id.edtMateria);
        edtFecha = (EditText)findViewById(R.id.edtFecha);
    }
    EditText edtID, edtNombre, edtMateria, edtFecha;

    public void onClick (View view){
        registrarUsuarios();
    }

    private void registrarUsuarios() {
        conexionSQLiteHelper con = new conexionSQLiteHelper(this,"bd_usuarios", null,1);

        SQLiteDatabase db = con.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(utilidades.CAMPO_ID, edtID.getText().toString());
        values.put(utilidades.CAMPO_NOMBRE, edtNombre.getText().toString());
        values.put(utilidades.CAMPO_MATERIA, edtMateria.getText().toString());
        values.put(utilidades.CAMPO_FECHA, edtFecha.getText().toString());

        long idresultado = db.insert(utilidades.TABLA_USUARIO, utilidades.CAMPO_ID,values);

        Toast.makeText(getApplicationContext(),"id Registro: "+idresultado,Toast.LENGTH_LONG).show();
        db.close();
    }


}
