package com.example.evotec.faccipmconstantepalacioscajabancaria;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.evotec.faccipmconstantepalacioscajabancaria.utilidades.utilidades;

public class ActivityRegistro extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        edtID = (EditText)findViewById(R.id.edtID);
        edtUbicacion = (EditText)findViewById(R.id.edtUbicacion);
        edtCedula = (EditText)findViewById(R.id.edtCedula);
        edtSaldo = (EditText)findViewById(R.id.edtSaldo);
    }
    EditText edtID, edtCedula, edtUbicacion, edtSaldo;

    public void onClick (View view){
        registrarUsuarios();
    }

    private void registrarUsuarios() {
        conexionSQLiteHelper con = new conexionSQLiteHelper(this,"bd_usuarios", null,1);

        SQLiteDatabase db = con.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(utilidades.CAMPO_IDCAJA, edtID.getText().toString());
        values.put(utilidades.CAMPO_UBICACIONCAJA, edtUbicacion.getText().toString());
        values.put(utilidades.CAMPO_CEDULACAJA, edtCedula.getText().toString());
        values.put(utilidades.CAMPO_SALDOINICIAL, edtSaldo.getText().toString());

        long idresultado = db.insert(utilidades.TABLA_USUARIO, utilidades.CAMPO_IDCAJA,values);

        Toast.makeText(getApplicationContext(),"REGISTRO NUMERO: "+idresultado,Toast.LENGTH_LONG).show();
        db.close();
    }
    public void modificarDatos (View view){
        modificar();
    }

    private void modificar() {
        conexionSQLiteHelper con = new conexionSQLiteHelper(this,

                "bd_usuarios", null, 1);

        SQLiteDatabase bd = con.getWritableDatabase();
        ContentValues values = new ContentValues();

        String id_caja = edtID.getText().toString();
        String ubicacion_caja = edtUbicacion.getText().toString();
        String cedula_caja = edtCedula.getText().toString();
        String saldo_caja = edtSaldo.getText().toString();






        // actualizamos con los nuevos datos, la información cambiada
        values.put("CAMPO_UBICACION", String.valueOf(edtUbicacion));
        values.put("CAMPO_CEDULACAJA", String.valueOf(edtCedula));
        values.put("CAMPO_SALDOINICIAL", String.valueOf(edtSaldo));

        int cant = bd.update("bd_usuarios", values, "id_caja=" + edtID, null);

        bd.close();

        if (cant == 1)

            Toast.makeText(this, "Datos modificados con éxito", Toast.LENGTH_SHORT)

                    .show();

        else

            Toast.makeText(this, "No existe usuario",

                    Toast.LENGTH_SHORT).show();

    }

}
