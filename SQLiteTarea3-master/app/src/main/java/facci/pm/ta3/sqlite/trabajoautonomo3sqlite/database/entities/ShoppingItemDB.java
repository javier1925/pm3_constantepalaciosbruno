package facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.entities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.provider.SyncStateContract;

import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.helper.ShoppingElementHelper;
import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.model.ShoppingItem;
import java.util.ArrayList;

public class ShoppingItemDB {

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private ShoppingElementHelper dbHelper;

    public ShoppingItemDB(Context context) {
        // SE CREA EL ELEMENTO HELPER
        dbHelper = new ShoppingElementHelper(context);
    }

    /* Inner class that defines the table contents */
    public static abstract class ShoppingElementEntry implements BaseColumns {
        public static final String TABLE_NAME = "entry";
        public static final String COLUMN_NAME_TITLE = "title";

// CREAR Y ELIMINAN UNA TABLA
        public static final String CREATE_TABLE = "CREATE TABLE " +
                TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
                COLUMN_NAME_TITLE + TEXT_TYPE + " )";
        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;


    }


    public void insertElement(String productName) {

        //NOS PERMITE MOSTRAR DATON QUE ESTAN UBICADOS EN LA BD
        //TODO: Todo el código necesario para INSERTAR un Item a la Base de datos
        SQLiteDatabase db = dbHelper.getWritableDatabase(); //OBTIENE DATOS DE DEPOSITO
        ContentValues values = new ContentValues();        //Pasamos un ContentValues objeto al insert()
        values.put(ShoppingElementEntry.COLUMN_NAME_TITLE, productName); //Crear un nuevo mapa de valores, donde los nombres de columna son las claves
        db.insert(ShoppingElementEntry.TABLE_NAME,ShoppingElementEntry.COLUMN_NAME_TITLE,values);
        db.close(); //Cerrar nuestra db

    }


    public ArrayList<ShoppingItem> getAllItems() {

        ArrayList<ShoppingItem> shoppingItems = new ArrayList<>();

        String[] allColumns = { ShoppingElementEntry._ID,
            ShoppingElementEntry.COLUMN_NAME_TITLE};

        Cursor cursor = dbHelper.getReadableDatabase().query(
            ShoppingElementEntry.TABLE_NAME,    // The table to query (TABLA PARA CONSULTA)
            allColumns,                         // The columns to return   (COLUMNAS PARA RETORNAR)
            null,                               // The columns for the WHERE clause      (COLUMNAS PARA LA CALUSULA WHERE)
            null,                               // The values for the WHERE clause     (VALORES PARA LA CLAUSULA WHERE)
            null,                               // don't group the rows     (NO AGRUPACION DE FILAS)
            null,                               // don't filter by row groups    (FILTRAR POR GRUPOS DE FILA)
            null                                // The sort order      (EL ORDEN DE CLASIFICACION)

                //Los argumentos( selection y selectionArgs) se combinan para crear una cláusula WHERE.
        );

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ShoppingItem shoppingItem = new ShoppingItem(getItemId(cursor), getItemName(cursor));
            shoppingItems.add(shoppingItem);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        dbHelper.getReadableDatabase().close();
        return shoppingItems;
    }

    private long getItemId(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(ShoppingElementEntry._ID));
    }

    private String getItemName(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndexOrThrow(ShoppingElementEntry.COLUMN_NAME_TITLE));
    }


    public void clearAllItems() {


        //TODO: Todo el código necesario para ELIMINAR todos los Items de la Base de datos
SQLiteDatabase db=dbHelper.getWritableDatabase(); //Obtiene el depósito de datos en modo de escritura
db.delete(ShoppingElementEntry.TABLE_NAME, "", null);//Codigo para la eliminacion de todos los Items de la base de datos
db.close(); //cerrar nuestra db


    }

    public void updateItem(ShoppingItem shoppingItem) {


        //TODO: Todo el código necesario para ACTUALIZAR un Item en la Base de datos
        SQLiteDatabase db = dbHelper.getWritableDatabase();//Obtiene el depósito de datos en modo de escritura
        ContentValues values = new ContentValues();             //Pasamos un ContentValues objeto al updateItem()
        values.put(ShoppingElementEntry.COLUMN_NAME_TITLE, //fila a actualizar por el titulo
                shoppingItem.getName());
        db.update(ShoppingElementEntry.TABLE_NAME,values,  //actualizando la lista por el nombre y el ID
                ShoppingElementEntry._ID + " = "+ shoppingItem.getId(),
                null);
        db.close(); //cerrar nuestra db


    }

    public void deleteItem(ShoppingItem shoppingItem) {


        //TODO: Todo el código necesario para ELIMINAR un Item de la Base de datos
        SQLiteDatabase db = dbHelper.getWritableDatabase(); //Obtiene el depósito de datos en modo de escritura
        String  ctgFer=  ShoppingElementEntry._ID + " = "+ shoppingItem.getId();  //Tomando el ID.
       db.delete(ShoppingElementEntry.TABLE_NAME, //Eliminacion de un item Se realiza cobteniendo el ID Y  el nombre de la tabla.
               ctgFer,null);
        db.close();  //cerrar nuestra db


    }
}
//ECHO POR BRUNO JAVIER CONSTANTE PALACIOS
