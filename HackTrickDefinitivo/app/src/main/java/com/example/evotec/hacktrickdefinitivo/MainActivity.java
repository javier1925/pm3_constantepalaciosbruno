package com.example.evotec.hacktrickdefinitivo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnModoSuperu=(Button)findViewById(R.id.btnModoSuperu);
        btnModoEspec=(Button)findViewById(R.id.btnModoEspec);

        btnModoEspec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActivityEstadistica.class);
                startActivity(intent);

            }
        });
        btnModoSuperu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(MainActivity.this, ActivityLogin.class);
                startActivity(intent2);

            }
        });

    }
    Button btnModoSuperu, btnModoEspec;
}
