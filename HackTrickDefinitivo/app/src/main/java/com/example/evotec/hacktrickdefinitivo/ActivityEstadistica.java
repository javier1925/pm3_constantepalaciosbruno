package com.example.evotec.hacktrickdefinitivo;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ActivityEstadistica extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estadistica);
        btnBusquedaCampeon=(Button)findViewById(R.id.btnBusquedaCampeon);
        btnRevisarEstad = (Button)findViewById(R.id.btnRevisarEstad);
        btnflecha = (Button)findViewById(R.id.btnflecha);


        btnRevisarEstad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityEstadistica.this, ActivityRevisarEstado.class);
                startActivity(intent);

            }
        });
        btnBusquedaCampeon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(ActivityEstadistica.this, ActivityBuscarCampeonatos.class);
                startActivity(intent2);

            }
        });

        btnflecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new  Intent (ActivityEstadistica.this, MainActivity.class);
                startActivity(intent);

            }
        });


    }
    Button btnBusquedaCampeon, btnflecha, btnRevisarEstad;
}
