package com.example.evotec.hacktrickdefinitivo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ActivityModoSuper extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modo_super);
        btnActualizarCampeonatos = (Button)findViewById(R.id.btnActualizarCampeonatos);
        btnGenerarCampeonatos = (Button)findViewById(R.id.btnGenerarCampeonatos);
        btnCrearCampeonatos = (Button)findViewById(R.id.btnCrearCampeonatos);
        btnflecha = (Button)findViewById(R.id.btnflecha);

        btnActualizarCampeonatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityModoSuper.this, ActivityActualizar.class);
                startActivity(intent);

            }
        });
        btnGenerarCampeonatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(ActivityModoSuper.this, ActivityRevisar.class);
                startActivity(intent2);

            }
        });
        btnCrearCampeonatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityModoSuper.this, ActivityCrear.class);
                startActivity(intent);

            }
        });
        btnflecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new  Intent (ActivityModoSuper.this, MainActivity.class);
                startActivity(intent);

            }
        });



    }
    Button btnActualizarCampeonatos, btnCrearCampeonatos, btnGenerarCampeonatos, btnflecha;
}
