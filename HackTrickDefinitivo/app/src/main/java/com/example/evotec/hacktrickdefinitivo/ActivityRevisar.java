package com.example.evotec.hacktrickdefinitivo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ActivityRevisar extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_revisar);
        //ACTIVIDAD QUE GENERA LOS PARTIDOS ALEATOREAMENTE O MANUALMENTE
        btnAleatoreo = (Button)findViewById(R.id.btnAleatorio);
        btnGenerarCamp = (Button)findViewById(R.id.btnGenerarCamp);
        btnflecha = (Button)findViewById(R.id.btnflecha);

        edtSequipo1 = (EditText)findViewById(R.id.edtSequipo1);
        edtSequipo2 = (EditText)findViewById(R.id.edtSequipo2);
        edtSequipo3 = (EditText)findViewById(R.id.edtSequipo3);
        edtSequipo4 = (EditText)findViewById(R.id.edtSequipo4);

        btnflecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new  Intent (ActivityRevisar.this, ActivityModoSuper.class);
                startActivity(intent);

            }
        });


    }

    EditText edtSequipo1, edtSequipo2, edtSequipo3, edtSequipo4;
    Button btnAleatoreo, btnGenerarCamp, btnflecha;
}
