package com.example.evotec.pruebamovil;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnCrear=(Button)findViewById(R.id.btnCrear);
        btnRevisar=(Button)findViewById(R.id.btnRevisar);
        btnEstadisticas=(Button)findViewById(R.id.btnEstadisticas);

        btnCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActivityCrear.class);
                startActivity(intent);

            }
        });
        btnRevisar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(MainActivity.this, ActivityRevisar.class);
                startActivity(intent2);

            }
        });
        btnEstadisticas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent3 = new Intent(MainActivity.this, ActivityEstadistica.class);
                startActivity(intent3);

            }
        });

    }
    Button btnCrear, btnRevisar, btnEstadisticas;
}
